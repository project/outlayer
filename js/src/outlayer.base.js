/**
 * @file
 * Provides GridStack Outlayer base shared utility methods.
 */

(function ($, Drupal) {

  'use strict';

  var noop = function () {};

  $.gridstack = $.gridstack || {};
  $.gridstack.base = $.gridstack.base || {};

  /**
   * Outlayer utility functions.
   *
   * @namespace
   */
  $.outLayer = $.extend({}, $.gridstack.base || {}, {
    useTransition: true,
    forceHeight: false,
    onResizeEnd: noop,

    /**
     * Updates grid boxes with the given aspect ratios to make them responsive.
     *
     * Basically avoids hard-coding widths and heights thanks to padding hack.
     * This is one good reason to avoid using Imagesloaded plugin since we
     * don't have to wait for any image to finish loading to have a grid layout.
     * Consider this as a frame for a picture image.
     *
     * @param {HTMLElement} el
     *   The gridstack HTML element.
     * @param {Event} e
     *   The optional resize event.
     */
    updateRatioMultiple: function (el, e) {
      var me = this;

      // Resizing needs fresh non-cached items to support multiple instances.
      var items = typeof e === 'undefined' || e === true
        ? me.$items : $.findAll(el, me.itemSelector);

      if (items.length) {
        $.each(items, function (item) {
          me.updateRatio(item, el, e);
        });
      }
    },

    rePosition: function (el, reset) {
      var me = this;

      /*
       * Masonry/ Packery does the reverse during resizing:
       * makes .box__content absolute which should be relative,
       * and makes .box relative which should be absolute.
       * Without resizing, they behave as expected.
       *
       * This basically lets CSS manage positioning instead.
       * The reason: native Grid wants relative, js-driven layouts absolute.
       * Isotope allows them to interchange before/ after filtering.
       */
      var resetItem = function (box) {
        // Ensures to not touch display, required by filtering.
        box.style.position = box.style.left = box.style.top = box.style.transform = '';
      };

      $.each(me.$items, function (item) {
        var content = $.find(item, '.box__content');

        el.style.position = item.style.position = '';

        if ($.isElm(content)) {
          content.style.position = '';

          // Fixed for the new CSS Grid during filtering, sorting, searching.
          if (me.forceHeight && item.offsetHeight > 600) {
            content.style.maxHeight = item.offsetHeight + 'px';
          }
        }

        if (reset) {
          resetItem(item);
        }

        // Removes Masonry/Packery/isotope inline styles when disabled.
        if (!me.isEnabled(el)) {
          resetItem(item);

          el.style.height = el.style.minHeight = '';
        }

        if (!$.attr(item, 'style')) {
          $.removeAttr(item, 'style');
        }
      });
    },

    updateGrid: function (el, e) {
      var me = this;

      me.updateClasses(el, e);
      me.updateRatioMultiple(el, e);

      if (!$.isUnd(e)) {
        me.onResizeEnd(e, el);
      }
    },

    prepare: function (el) {
      var me = this;

      me.initials(el);
    },

    onTransitionEnd: function (e) {
      var me = this;
      var el = e.target;

      // Don't do anything fancy to not break transitions during filtering.
      if (me.$instance && $.hasClass(el, 'outlayer--' + me.onClass)) {
        me.$instance.layout();

        me.rePosition(el);
      }
    },

    init: function (el) {
      var me = this;

      el = el || me.$el;

      me.load(el);

      // Cheaper alternative to imagesloaded, delaying the layout
      // until the aspect ratio is set up via ::update() and ::buildOut().
      // This is no big deal if native Grid is on since it requires relative
      // positioning. This might also mess up Isotope filtering, beware.
      if (me.useTransition) {
        $.on(el, 'transitionend', me.onTransitionEnd.bind(me), false);
        me.useTransition = false;
      }
    }

  });

}(dBlazy, Drupal));
