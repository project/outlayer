/**
 * @file
 * Provides Outlayer Masonry loader.
 */

(function ($, Drupal, Masonry) {

  'use strict';

  var _id = 'outlayer';
  var _plugin = 'masonry';
  var _idOnce = _id + '-' + _plugin;
  var _mounted = 'is-' + _plugin + '-on';
  var _selector = '.' + _id + '--' + _plugin + ':not(.' + _mounted + ')';

  /**
   * Outlayer utility functions.
   *
   * @namespace
   */
  $.outLayer.masonry = $.extend($.outLayer || {}, {

    onClass: 'masonry',

    load: function (el) {
      var me = this;

      me.$el = el;
      me.options = $.parse($.attr(el, 'data-' + _idOnce));

      // Prepare, define aspect ratio before laying out, and initialize.
      me.prepare(el);

      me.$instance = new Masonry(el, me.options);

      // Runs update(), including on resize event, only if nobody helps.
      if (me.isUnGridStack) {
        me.buildOut(el);
      }

      me.cleanUp(el);
    }

  });

  /**
   * Attaches Outlayer behavior to HTML element.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.outLayerMasonry = {
    attach: function (context) {
      var me = $.outLayer.masonry;
      $.once(me.init.bind(me), _idOnce, _selector, context);
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        $.once.removeSafely(_idOnce, _selector, context);
      }
    }
  };

}(dBlazy, Drupal, Masonry));
