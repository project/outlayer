<?php

namespace Drupal\outlayer_ui\Form;

use Drupal\Core\Url;
use Drupal\blazy\Form\BlazyDeleteFormBase;

/**
 * Builds the form to delete a Outlayer optionset.
 */
class OutlayerDeleteForm extends BlazyDeleteFormBase {

  /**
   * Defines the nice anme.
   *
   * @var string
   */
  protected static $niceName = 'Outlayer';

  /**
   * Defines machine name.
   *
   * @var string
   */
  protected static $machineName = 'outlayer';

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.outlayer.collection');
  }

}
