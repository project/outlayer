<?php

namespace Drupal\outlayer\Plugin\views\style;

use Drupal\blazy\Views\BlazyStylePluginBase;
use Drupal\outlayer\OutlayerDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base Outlayer style plugin.
 */
abstract class OutlayerViewsBase extends BlazyStylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'outlayer';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $captionId = 'caption';

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * The outlayer service manager.
   *
   * @var \Drupal\outlayer\OutlayerManagerInterface
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->manager = $container->get('outlayer.manager');
    $instance->blazyManager = $instance->blazyManager ?? $container->get('blazy.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function manager() {
    return $this->manager;
  }

  /**
   * {@inheritdoc}
   */
  public function admin() {
    return \Drupal::service('outlayer.admin');
  }

  /**
   * Returns item list suitable for button groups.
   */
  public function buildItemList(array $items, array $settings, $type = 'sorter') {
    $outlayers = $settings['outlayers'];
    $id = $outlayers->get('view.based_id') ?: $settings['instance_id'];
    return [
      '#theme' => 'item_list',
      '#items' => $items,
      '#context' => ['settings' => $settings],
      '#attributes' => [
        'class' => [
          'outlayer-list',
          'outlayer-list--' . $type,
          'btn-group',
        ],
        'data-instance-id' => $id,
      ],
      '#wrapper_attributes' => [
        'class' => ['item-list--outlayer', 'item-list--outlayer-' . $type],
      ],
    ];
  }

  /**
   * Provides commons settings for the style plugins.
   */
  protected function buildSettings() {
    $settings  = parent::buildSettings();
    $settings += OutlayerDefault::htmlSettings();
    $blazies   = $settings['blazies'];

    $blazies->set('item.id', static::$itemId)
      ->set('item.prefix', static::$itemPrefix)
      ->set('item.caption', static::$captionId)
      ->set('namespace', static::$namespace)
      ->set('is.noratio', TRUE);

    return $settings;
  }

}
