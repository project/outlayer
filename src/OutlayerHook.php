<?php

namespace Drupal\outlayer;

use Drupal\Core\File\FileSystem;
use Drupal\Core\Template\Attribute;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Outlayer utility methods for Drupal hooks.
 */
class OutlayerHook implements OutlayerHookInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The outlayer manager service.
   *
   * @var \Drupal\outlayer\OutlayerManagerInterface
   */
  protected $manager;

  /**
   * The library info definition.
   *
   * @var array
   */
  protected $libraryInfoBuild;

  /**
   * Constructs a Outlayer object.
   *
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The file system service.
   * @param \Drupal\outlayer\OutlayerManagerInterface $manager
   *   The outlayer manager service.
   */
  public function __construct(FileSystem $file_system, OutlayerManagerInterface $manager) {
    $this->fileSystem = $file_system;
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('outlayer.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function button(array $variables): array {
    $title      = $variables['title'];
    $classes    = ['btn', 'button', 'button--outlayer'];
    $attributes = new Attribute();

    if (!empty($variables['filter'])) {
      $attributes->setAttribute('data-filter', $variables['filter']);
      $classes[] = 'btn-primary';
    }
    if (!empty($variables['sorter'])) {
      $attributes->setAttribute('data-sort-by', $variables['sorter']);
      $classes[] = 'btn-secondary';
    }

    $classes = array_merge($classes, $variables['classes']);
    $attributes->addClass($classes);
    $attributes->setAttribute('type', 'button');

    return [
      '#markup' => '<button' . $attributes . '>' . $title . '</button>',
      '#allowed_tags' => ['button', 'span'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function configSchemaInfoAlter(array &$definitions): void {
    if (isset($definitions['outlayer_base'])) {
      $this->manager->configSchemaInfoAlter($definitions, 'outlayer_base', OutlayerDefault::extendedSettings());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function libraryInfoBuild(): array {
    if (!isset($this->libraryInfoBuild)) {
      $libraries = [];
      foreach (array_values(OutlayerDefault::extraLayouts()) as $id) {
        if ($library = $this->manager->getLibrariesPath('isotope-' . $id)) {
          $filename = $id == 'packery' ? $id . '-mode.pkgd' : $id;
          $ext = is_file($library . '/' . $filename . '.min.js') ? 'min.js' : 'js';

          $libraries['isotope-' . $id]['js']['/' . $library . '/' . $filename . '.' . $ext] = ['weight' => -2];
          $libraries['isotope-' . $id]['dependencies'][] = 'outlayer/isotope';
        }
      }
      $this->libraryInfoBuild = $libraries;
    }

    return $this->libraryInfoBuild;
  }

  /**
   * {@inheritdoc}
   */
  public function libraryInfoAlter(array &$libraries, $extension): void {
    if ($extension === 'outlayer' && $this->manager->getLibrariesPath('outlayer')) {
      // The main library.
      // @todo remove checks once the library has minified versions.
      if ($library = $this->manager->getLibrariesPath('outlayer')) {
        foreach (['item', 'outlayer'] as $name) {
          $ext = is_file($library . '/' . $name . '.min.js') ? 'min.js' : 'js';
          $libraries['outlayer']['js']['/' . $library . '/' . $name . '.' . $ext] = ['weight' => -6];
        }
      }

      // Layouts based on outlayer library.
      foreach (['isotope', 'masonry', 'packery'] as $id) {
        // Composer based libraries suffixed with `-layout`.
        $library = $this->manager->getLibrariesPath($id) ?: $this->manager->getLibrariesPath($id . '-layout');
        if ($library) {
          foreach ($libraries[$id]['js'] as $uri => $info) {
            $basename = $this->fileSystem->basename($uri);
            // @todo when we do not use .pkgd.min.js, extract from sources:
            // @todo $library = $id == 'masonry' ? $library : $library . '/js';
            // @todo $library = strpos($uri, 'layout-modes') !== FALSE ? $library . '/layout-modes' : $library;
            // @todo $libraries[$id]['js']['/' . $library . '/' . $basename] = $info;
            $libraries[$id]['js']['/' . $library . '/dist/' . $basename] = $info;
          }
        }
      }

      // Isotope extra layouts prefixed with `isotope-`.
      foreach (OutlayerDefault::extraLayouts() as $id) {
        $filename = $id == 'packery' ? $id . '-mode.pkgd' : $id;

        if ($library = $this->manager->getLibrariesPath('isotope-' . $id)) {
          $ext = is_file($library . '/' . $filename . '.min.js') ? 'min.js' : 'js';
          $libraries['isotope-' . $id]['js']['/' . $library . '/' . $filename . '.' . $ext] = ['weight' => -2];
        }
      }
    }
  }

}
